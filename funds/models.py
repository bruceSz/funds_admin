from django.db import models


# Create your models here.
class Fund(models.Model):
    title = models.CharField(max_length=100)
    invest_cycle = models.CharField(max_length=30)
    expected_profit_rate = models.CharField(max_length=10)
    lowest_amount = models.CharField(max_length=10)
    risk_type = models.CharField(max_length=10)
    description = models.CharField(max_length=1000)
    invest_type = models.CharField(max_length=10)

    class Meta:
        db_table='funds'
